use std::path::Path;
use std::fs::read_to_string;

pub fn twop1(is_test: bool)   {
    let name: &str;
    if is_test  {
        name = "./inputs/input-2-test";
    }
    else    {
        name = "./inputs/input-2";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut horizontal = 0;
    let mut depth = 0;
    for mv in input.lines()   {
        let mut split = mv.split(" ");
        let dir = split.next().unwrap();
        let len = split.next().unwrap().parse::<usize>().unwrap();
        match dir   {
            "forward" => { horizontal += len },
            "up" => { depth -= len },
            "down" => { depth += len },
            _ => panic!("malformed input")
        }
    }
    println!("{}", depth * horizontal);
}

pub fn twop2(is_test: bool)   {
    let name: &str;
    if is_test  {
        name = "./inputs/input-2-test";
    }
    else    {
        name = "./inputs/input-2";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;
    for mv in input.lines()   {
        let mut split = mv.split(" ");
        let dir = split.next().unwrap();
        let len = split.next().unwrap().parse::<usize>().unwrap();
        match dir   {
            "forward" => { horizontal += len; depth += len * aim },
            "up" => { aim -= len },
            "down" => { aim += len },
            _ => panic!("malformed input")
        }
    }
    println!("{}", depth * horizontal);
}
