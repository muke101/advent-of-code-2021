use std::cmp::Ordering;
use std::path::Path;
use std::fs::read_to_string;
use std::collections::BinaryHeap;

#[derive(Eq, PartialEq, Copy, Clone)]
struct TentativeDistance	{
	coord: (usize, usize),
	distance: u32
}

impl PartialOrd for TentativeDistance	{
	fn partial_cmp(&self, other: &Self) -> Option<Ordering>	{
		Some(self.cmp(other))
	}
}

impl Ord for TentativeDistance	{
	fn cmp(&self, other: &Self) -> Ordering	{
        std::cmp::Reverse(self.distance).cmp(&std::cmp::Reverse(other.distance))
	}
}

pub fn fithteenp1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-15-test";
    }
    else    {
        name = "./inputs/input-15";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let grid = input.lines()
                    .map(|l| l.chars()
                         .map(|c| c.to_digit(10)
                              .unwrap())
                         .collect::<Vec<u32>>())
                    .collect::<Vec<_>>();
    let mut visited = grid.iter().map(|l| l.iter().map(|_| false).collect::<Vec<_>>()).collect::<Vec<_>>();
    let mut lowest: BinaryHeap<TentativeDistance> = BinaryHeap::new();
    let y_max = grid.len();
    let x_max = grid[0].len();

    visited[0][0] = true;
    lowest.push(TentativeDistance { coord: (0,0), distance: 0 });

	let final_distance: u32;
    'outer: loop {
        let node = lowest.pop().unwrap();
        let (x,y) = node.coord;
		let x_n = x as isize;
		let y_n = y as isize;
        let neighbors = [(x_n-1,y_n),(x_n,y_n-1),(x_n+1,y_n),(x_n,y_n+1)]
			.iter()
            .filter(|(x,y)|
                    *x >= 0 && *x < x_max as isize &&
                    *y >= 0 && *y < y_max as isize &&
                    !visited[*y as usize][*x as usize])
            .map(|(x,y)| (*x as usize, *y as usize))
            .collect::<Vec<_>>();

        for (x,y) in neighbors	{
            let new_distance = grid[y][x] + node.distance;
            if x == x_max-1 && y == y_max-1	{
				final_distance = new_distance;
				break 'outer;
			}
            lowest.push(TentativeDistance { coord: (x,y), distance: new_distance });
            visited[y][x] = true;
        }
    }
	println!("{}", final_distance);
}

pub fn fithteenp2(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-15-test";
    }
    else    {
        name = "./inputs/input-15";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let sub_grid = input.lines()
                    .map(|l| l.chars()
                         .map(|c| c.to_digit(10)
                              .unwrap())
                         .collect::<Vec<u32>>())
                    .collect::<Vec<_>>();
    let sub_y_max = sub_grid.len();
    let sub_x_max = sub_grid[0].len();
    let y_max = sub_grid.len()*5;
    let x_max = sub_grid[0].len()*5;

	let mut grid: Vec<Vec<u32>> = vec![vec![0; x_max]; y_max];
    let mut visited = grid.iter().map(|l| l.iter().map(|_| false).collect::<Vec<_>>()).collect::<Vec<_>>();
    let mut lowest: BinaryHeap<TentativeDistance> = BinaryHeap::new();

	for i in 0..5	{
		let y_lower = sub_y_max*i;
		let y_upper = sub_y_max*(i+1);
		for j in 0..5	{
			let x_lower = sub_x_max*j;
			let x_upper = sub_x_max*(j+1);
			grid[y_lower..y_upper].iter_mut().enumerate().for_each(|(c, l)| l[x_lower..x_upper].clone_from_slice(&sub_grid[c]));
			grid[y_lower..y_upper].iter_mut().for_each(|l| increment_row(&mut l[x_lower..x_upper], i, j));
		}
	}

    visited[0][0] = true;
    lowest.push(TentativeDistance { coord: (0,0), distance: 0 });

	let final_distance: u32;
    'outer: loop {
        let node = lowest.pop().unwrap();
        let (x,y) = node.coord;
		let x_n = x as isize;
		let y_n = y as isize;
        let neighbors = [(x_n-1,y_n),(x_n,y_n-1),(x_n+1,y_n),(x_n,y_n+1)]
			.iter()
            .filter(|(x,y)|
                    *x >= 0 && *x < x_max as isize &&
                    *y >= 0 && *y < y_max as isize &&
                    !visited[*y as usize][*x as usize])
            .map(|(x,y)| (*x as usize, *y as usize))
            .collect::<Vec<_>>();

        for (x,y) in neighbors	{
            let new_distance = grid[y][x] + node.distance;
            if x == x_max-1 && y == y_max-1	{
				final_distance = new_distance;
				break 'outer;
			}
            lowest.push(TentativeDistance { coord: (x,y), distance: new_distance });
            visited[y][x] = true;
        }
    }
	println!("{}", final_distance);
}

fn increment_row(row: &mut [u32], i: usize, j: usize)	{
	let increment = (i+j) as u32;
	row.iter_mut().for_each(|n| *n = (*n + increment - 1) % 9 + 1);
}
