use std::path::Path;
use std::fs::read_to_string;
use itertools::Itertools;

struct Point	{
	x: isize,
	y: isize
}

pub fn seventeenp1(is_test: bool)	{
    let name: &str;
    if is_test	{
        name = "./inputs/input-17-test";
    }
    else    {
        name = "./inputs/input-17";
    }
    let p = Path::new(name);
    let input = &read_to_string(p).unwrap()[13..];
    let (x_bounds, y_bounds) = input.split(", ").collect_tuple().unwrap();
    let (x_min, x_max) = x_bounds.split("=").nth(1).unwrap().split("..").map(|n| n.parse::<isize>().unwrap()).collect_tuple().unwrap();
	let y_bounds = y_bounds.strip_suffix("\n").unwrap();
    let (y_min, y_max) = y_bounds.split("=").nth(1).unwrap().split("..").map(|n| n.parse::<isize>().unwrap()).collect_tuple().unwrap();

	let c = -(x_min+(x_max-x_min)/2)*2;
	let determinate = (((1 - 4*c) as f64).sqrt()).round() as usize;
	let initial_x = ((1 + determinate)/2) as isize;
	let initial_y = (initial_x as isize + y_max).abs();

	println!("{}", simulate_step(Point { x: 9, y: initial_y }, Point { x: 0, y: 0 }, Point { x: x_min, y: y_min }, Point { x: x_max, y: y_max }, 0).0);

}

pub fn seventeenp2(is_test: bool)	{
    let name: &str;
    if is_test	{
        name = "./inputs/input-17-test";
    }
    else    {
        name = "./inputs/input-17";
    }
    let p = Path::new(name);
    let input = &read_to_string(p).unwrap()[13..];
    let (x_bounds, y_bounds) = input.split(", ").collect_tuple().unwrap();
    let (x_min, x_max) = x_bounds.split("=").nth(1).unwrap().split("..").map(|n| n.parse::<isize>().unwrap()).collect_tuple().unwrap();
	let y_bounds = y_bounds.strip_suffix("\n").unwrap();
    let (y_min, y_max) = y_bounds.split("=").nth(1).unwrap().split("..").map(|n| n.parse::<isize>().unwrap()).collect_tuple().unwrap();


    let min_bounds = Point { x: 0, y: y_min };
    let max_bounds = Point { x: x_max*3, y: y_max*-3 };

	let mut velocities: Vec<(isize, isize)> = vec![];
    for v in min_bounds.y..max_bounds.y	{
		for u in min_bounds.x..max_bounds.x	{
			if simulate_step(Point { x: u, y: v }, Point { x: 0, y: 0 }, Point { x: x_min, y: y_min }, Point { x: x_max, y: y_max }, 0).0	{
				velocities.push((u,v));
			}
		}
    }

	println!("{}", velocities.len());

}

fn find_max_y(initial_x: isize, initial_y: isize, x_max: isize, x_min: isize, y_min: isize, y_max: isize) -> isize	{

	let mut possible_y = initial_y + 50;
	let mut inbounds = simulate_step(Point { x: initial_x, y: possible_y }, Point { x: 0, y: 0 }, Point { x: x_min, y: y_min }, Point { x: x_max, y: y_max }, 0).0;

	while inbounds	{
		possible_y += 50;
		inbounds = simulate_step(Point { x: initial_x, y: possible_y }, Point { x: 0, y: 0 }, Point { x: x_min, y: y_min }, Point { x: x_max, y: y_min }, 0).0;
	}

	let mut upper_bound = possible_y;
	let mut lower_bound = possible_y-50;

	while upper_bound - lower_bound > 1	{
		possible_y = (((upper_bound+lower_bound) as f64)/2.0).floor() as isize;
		let inbounds = simulate_step(Point { x: initial_x, y: possible_y }, Point { x: 0, y: 0 }, Point { x: x_min, y: y_min }, Point { x: x_max, y: y_max }, 0).0;
		if inbounds	{
			lower_bound = possible_y;
		}
		else {
			upper_bound = possible_y;
		}
	}
	possible_y = (((upper_bound+lower_bound) as f64)/2.0).floor() as isize;
    return possible_y;
}

fn simulate_step(velocity: Point, current: Point, min_bound: Point, max_bound: Point, max_y: isize) -> (bool, isize)	{
	let new_x = current.x + velocity.x;
	let new_y = current.y + velocity.y;
	let new_u = match velocity.x	{
		v if v < 0 => v + 1,
		v if v > 0 => v - 1,
		_ => 0
	};
	let new_v = velocity.y - 1;
    let new_max_y = std::cmp::max(max_y, new_y);
	if new_y >= min_bound.y && new_y <= max_bound.y && new_x >= min_bound.x && new_x <= max_bound.x {
		return (true, new_max_y);
	}
    else if new_y < min_bound.y	{
        return (false, 0);
    }

	return simulate_step(Point { x: new_u, y: new_v }, Point { x: new_x, y: new_y }, min_bound, max_bound, new_max_y);

}
