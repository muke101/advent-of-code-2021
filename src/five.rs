use std::path::Path;
use std::fs::read_to_string;
use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
struct Point    {
    x: usize,
    y: usize
}

#[derive(Debug)]
struct Line {
    start: Point,
    end: Point
}

pub fn fivep1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-5-test";
    }
    else    {
        name = "./inputs/input-5";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines();
    let mut lines: Vec<Line> = vec![];

    for line in input_lines   {
        let mut coords = line.split(" -> ");
        let mut start = coords.next().unwrap().split(",");
        let mut end = coords.next().unwrap().split(",");
        let x1 = start.next().unwrap().parse::<usize>().unwrap();
        let y1 = start.next().unwrap().parse::<usize>().unwrap();
        let x2 = end.next().unwrap().parse::<usize>().unwrap();
        let y2 = end.next().unwrap().parse::<usize>().unwrap();
        lines.push(Line{ start: Point { x: x1, y: y1 }, end: Point { x: x2, y: y2 }});
    }

    let mut coord_map: HashMap<(usize, usize), usize> = HashMap::new();

    for line in lines.iter()    {
        let points = get_line_coords(&line.start, &line.end);
        for point in points.iter()  {
            let new_count = match coord_map.get(&(point.x, point.y)) {
                Some(count) => count+1,
                None => 1
            };
            coord_map.insert((point.x, point.y), new_count);
        }
    }

    println!("{}", coord_map.values().filter(|v| **v > 1).collect::<Vec<_>>().len())
}

fn get_line_coords(start: &Point, end: &Point) -> Vec<Point>  {
    let delta_y = end.y as isize - start.y as isize;
    let delta_x = end.x as isize - start.x as isize;
    return match delta_y   {
        0 => get_bounds(delta_x, start.x, end.x).map(|p| Point { x: p, y: start.y }).collect::<Vec<_>>(),
        _ if delta_x == 0 => get_bounds(delta_y, start.y, end.y).map(|p| Point { x: start.x, y: p }).collect::<Vec<_>>(),
        _ => vec![]
    };
}

fn get_bounds(delta: isize, start: usize, end: usize) -> std::ops::Range<usize>   {
    return match delta  {
        _ if delta > 0 => start..end+1,
        _ => end..start+1
    }
}

pub fn fivep2(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-5-test";
    }
    else    {
        name = "./inputs/input-5";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines();
    let mut lines: Vec<Line> = vec![];

    for line in input_lines   {
        let mut coords = line.split(" -> ");
        let mut start = coords.next().unwrap().split(",");
        let mut end = coords.next().unwrap().split(",");
        let x1 = start.next().unwrap().parse::<usize>().unwrap();
        let y1 = start.next().unwrap().parse::<usize>().unwrap();
        let x2 = end.next().unwrap().parse::<usize>().unwrap();
        let y2 = end.next().unwrap().parse::<usize>().unwrap();
        lines.push(Line{ start: Point { x: x1, y: y1 }, end: Point { x: x2, y: y2 }});
    }

    let mut coord_map: HashMap<(usize, usize), usize> = HashMap::new();

    for line in lines.iter()    {
        let points = get_line_coordsp2(line.start, line.end);
        for point in points.iter()  {
            let new_count = match coord_map.get(&(point.x, point.y)) {
                Some(count) => count+1,
                None => 1
            };
            coord_map.insert((point.x, point.y), new_count);
        }
    }

    println!("{}", coord_map.values().filter(|v| **v > 1).collect::<Vec<_>>().len())
}

fn get_line_coordsp2(start: Point, end: Point) -> Vec<Point>  {
    let delta_y = end.y as isize - start.y as isize;
    let delta_x = end.x as isize - start.x as isize;
    return match delta_y   {
        0 => get_bounds(delta_x, start.x, end.x).map(|p| Point { x: p, y: start.y }).collect::<Vec<_>>(),
        _ if delta_x == 0 => get_bounds(delta_y, start.y, end.y).map(|p| Point { x: start.x, y: p }).collect::<Vec<_>>(),
        _ => get_diagonal_bounds(delta_y, delta_x, start, end)
    };
}

fn get_diagonal_bounds(delta_y: isize, delta_x: isize, start: Point, end: Point) -> Vec<Point>  {
    let ystep: isize;
    let xstep: isize;
    match (delta_x > 0, delta_y > 0)    {
        (true, true) => { xstep = 1; ystep = 1; },
        (true, false) => { xstep = 1; ystep = -1; }
        (false, true) => { xstep = -1; ystep = 1; }
        (false, false) => { xstep = -1; ystep = -1; }
    }

    let mut bounds: Vec<Point> = vec![start];
    let mut previous = start;
    while previous.x != end.x  {
        let point = Point{ x: (previous.x as isize + xstep) as usize, y: (previous.y as isize + ystep) as usize };
        bounds.push(point);
        previous = point;
    }

    return bounds;
}
