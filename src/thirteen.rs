use std::path::Path;
use std::fs::read_to_string;
use std::collections::HashSet;

pub fn thirteenp1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-13-test";
    }
    else    {
        name = "./inputs/input-13";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut grid: HashSet<Vec<usize>> = input.lines()
									 .filter(|l| *l != "" && l.chars().next().unwrap().is_digit(10))
									 .map(|l| l.split(",")
										  .map(|n| n.parse::<usize>().unwrap())
										  .collect::<Vec<_>>())
                                     .collect::<HashSet<_>>();
    let fold = input.lines()
                    .filter(|l| l.len() > 9)
                    .map(|l| (l.chars().nth(11).unwrap(), l[13..].parse::<usize>().unwrap()))
                    .collect::<Vec<_>>()[0];
	let dir = match fold.0 { 'x' => 0, 'y' => 1, _ => panic!("malformed input") };
	let index = fold.1;
	let mut points: Vec<Vec<usize>> = vec![];
	for mut point in grid.drain()	{
		if point[dir] > index	{
			point[dir] -= 2*(point[dir] - index)
		}
		points.push(point);
	}
	points.iter().for_each(|p| { grid.insert(p.to_vec()); });
	println!("{}", grid.len());
}

pub fn thirteenp2(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-13-test";
    }
    else    {
        name = "./inputs/input-13";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut grid: HashSet<Vec<usize>> = input.lines()
									 .filter(|l| *l != "" && l.chars().next().unwrap().is_digit(10))
									 .map(|l| l.split(",")
										  .map(|n| n.parse::<usize>().unwrap())
										  .collect::<Vec<_>>())
                                     .collect::<HashSet<_>>();
    let folds = input.lines()
                    .filter(|l| l.len() > 9)
                    .map(|l| (l.chars().nth(11).unwrap(), l[13..].parse::<usize>().unwrap()))
                    .collect::<Vec<_>>();
    for fold in folds	{
        let dir = match fold.0 { 'x' => 0, 'y' => 1, _ => panic!("malformed input") };
        let index = fold.1;
		let mut points: Vec<Vec<usize>> = vec![];
        for mut point in grid.drain()	{
            if point[dir] > index	{
				point[dir] -= 2*(point[dir] - index)
            }
			points.push(point);
        }
		points.iter().for_each(|p| { grid.insert(p.to_vec()); });
    }

	let mut x_max = 0;
	let mut y_max = 0;

	for point in grid.iter()	{
		x_max = std::cmp::max(x_max, point[0]);
		y_max = std::cmp::max(y_max, point[1]);
	}

	let mut pretty_picture = vec![vec!['.'; x_max+1]; y_max+1];
	grid.iter().for_each(|p| pretty_picture[p[1]][p[0]] = '#');
	pretty_picture.iter().for_each(|l| println!("{:?}", l));
}
