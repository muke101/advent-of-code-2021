use std::path::Path;
use std::fs::read_to_string;
use std::collections::HashMap;
use itertools::Itertools;

pub fn fourteen(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-14-test";
    }
    else    {
        name = "./inputs/input-14";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut input_lines = input.lines();
    let polymer = input_lines.next().unwrap().chars().collect::<Vec<_>>();
    input_lines.next();
	let instructions = input_lines.map(|l| l.split(" -> ").collect_tuple().unwrap()).collect::<Vec<_>>();
	let mut template: HashMap<(char, char), char> = HashMap::new();
	instructions.iter().for_each(|(k, v)| { let mut k_chars = k.chars(); template.insert((k_chars.next().unwrap(), k_chars.next().unwrap()), v.chars().next().unwrap()); });
	let mut counter_map: HashMap<(char, char), usize> = HashMap::new();
	let mut char_counter_map: HashMap<char, usize> = HashMap::new();

	for i in 0..polymer.len()-1	{
		let first = polymer[i];
		let second = polymer[i+1];
		let insertion = template[&(first, second)];
		char_counter_map.entry(insertion).and_modify(|n| *n += 1).or_insert(1);
		char_counter_map.entry(first).and_modify(|n| *n += 1).or_insert(1);
		counter_map.entry((first, insertion)).and_modify(|n| *n += 1).or_insert(1);
		counter_map.entry((insertion, second)).and_modify(|n| *n += 1).or_insert(1);
	}
	char_counter_map.entry(polymer[polymer.len()-1]).and_modify(|n| *n += 1).or_insert(1);

	for _ in 0..39	{
        let mut new_map: HashMap<(char, char), usize> = HashMap::new();
		for (pair, count) in counter_map	{
			let (first, second) = pair;
			let insertion = template[&(first, second)];
			char_counter_map.entry(insertion).and_modify(|n| *n += count).or_insert(count);
			new_map.entry((first, insertion)).and_modify(|n| *n += count).or_insert(count);
			new_map.entry((insertion, second)).and_modify(|n| *n += count).or_insert(count);
		}
        counter_map = new_map;
	}

	let mut min = usize::MAX;
	let mut max = usize::MIN;
	for (_, v) in char_counter_map.iter()	{
		min = std::cmp::min(min, *v);
		max = std::cmp::max(max, *v);
	}
	println!("{}", max-min);
}
