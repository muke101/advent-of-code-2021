use std::path::Path;
use std::fs::read_to_string;

pub fn ninep1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-9-test";
    }
    else    {
        name = "./inputs/input-9";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines().collect::<Vec<_>>();
    let y_len = input_lines.len();
    let x_len = input_lines[0].chars().collect::<Vec<_>>().len();
    let mut grid = vec![vec![9; x_len+2]; y_len+2];

    for y in 1..y_len+1   {
        let values = input_lines[y-1].chars().map(|c| c.to_digit(10).unwrap()).collect::<Vec<_>>();
        for x in 1..x_len+1  {
            grid[y][x] = values[x-1];
        }
    }

	let mut lowest_points: Vec<u32> = vec![];

    for y in 1..y_len+1 {
        for x in 1..x_len+1 {
			if is_lowest(&grid, x, y)	{
				lowest_points.push(grid[y][x]+1);
			}
        }
    }

	println!("{}", lowest_points.iter().sum::<u32>());
}

fn is_lowest(grid: &Vec<Vec<u32>>, x1: usize, y1: usize) -> bool	{
	for y in [y1-1, y1+1].iter()	{
		if grid[y1][x1] >= grid[*y][x1]	{
			return false;
		}
	}
	for x in [x1-1, x1+1].iter()	{
		if grid[y1][x1] >= grid[y1][*x]	{
			return false;
		}
	}
	return true;
}

pub fn ninep2(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-9-test";
    }
    else    {
        name = "./inputs/input-9";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines().collect::<Vec<_>>();
    let y_len = input_lines.len();
    let x_len = input_lines[0].chars().collect::<Vec<_>>().len();
    let mut grid = vec![vec![9; x_len+2]; y_len+2];

    for y in 1..y_len+1   {
        let values = input_lines[y-1].chars().map(|c| c.to_digit(10).unwrap()).collect::<Vec<_>>();
        for x in 1..x_len+1  {
			if values[x-1] != 9	{
				grid[y][x] = 0;
			}
        }
    }

	let mut sizes: Vec<usize> = vec![];

	for y in 1..y_len	{
		for x in 1..x_len	{
			let cell = grid[y][x];
			if cell == 0	{
				let mut visited: usize = 0;
				search_basin(&mut grid, &mut visited, x, y);
				sizes.push(visited);
			}
		}
	}

	sizes.sort_unstable();
	let len = sizes.len();
	println!("{}", sizes[len-1]*sizes[len-2]*sizes[len-3]);
}

fn search_basin(grid: &mut Vec<Vec<u8>>, visited: &mut usize, x: usize, y: usize)	{
	grid[y][x] = 1;
	*visited += 1;
	let neighbors = vec![(x, y-1), (x, y+1), (x-1, y), (x+1, y)];
	for (x_n, y_n) in neighbors	{
		if grid[y_n][x_n] != 9 && grid[y_n][x_n] != 1	{
			search_basin(grid, visited, x_n, y_n);
		}
	}
}
