use std::path::Path;
use std::fs::read_to_string;

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
enum Op	{
	Value(usize),
	Sum,
	Product,
	Min,
	Max,
	GreaterThan,
	LessThan,
	Equal
}

pub fn sixteen(is_test: bool)	{
    let name: &str;
    if is_test  {
        name = "./inputs/input-16-test";
    }
    else    {
        name = "./inputs/input-16";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
	let input_string = input.lines().nth(0).unwrap();
    let binary = hex_to_bin(input_string);

	let mut ops: Vec<Op> = vec![];
	parse_version(&binary, &mut ops);
	if let Op::Value(res) = ops[0]	{
		println!("{}", res);
	}

}

fn hex_to_bin(hex: &str) -> String	{
	let mut binary = "".to_string();
    for hex_code in hex.chars()	{
		binary.push_str(match hex_code {
			'0' => "0000", '1' => "0001", '2' => "0010", '3' => "0011", '4' => "0100",
			'5' => "0101", '6' => "0110", '7' => "0111", '8' => "1000", '9' => "1001",
			'A' => "1010", 'B' => "1011", 'C' => "1100", 'D' => "1101", 'E' => "1110",
			'F' => "1111",
			_ => unreachable!(),
		});
    }
	return binary;
}

fn parse_bit_len<'a, 'b>(mut bin: &'b str, ops: &'a mut Vec<Op>) -> &'b str	{
	let len = bin_to_int(&bin[..15]) as usize;
	bin = &bin[15..];
	let original_len = bin.len();
	let mut new_len = original_len;

	while (original_len - new_len) < len	{
		bin = parse_version(&bin, ops);
		new_len = bin.len();
	}

	return bin;
}

fn parse_packet_len<'a, 'b>(mut bin: &'b str, ops: &'a mut Vec<Op>) -> &'b str	{
	let len = bin_to_int(&bin[..11]);
	bin = &bin[11..];

	for _ in 0..len	{
		bin = parse_version(&bin, ops);
	}

	return bin;
}

fn parse_len_id<'a, 'b>(bin: &'b str, ops: &'a mut Vec<Op>) -> &'b str	{
	let bin = match bin.chars().nth(0).unwrap()	{
		'0' => parse_bit_len(&bin[1..], ops),
		'1' => parse_packet_len(&bin[1..], ops),
		_ => unreachable!()
	};
	return bin;
}

fn parse_part(bin: &str) -> (bool, &str)	{
	let is_final = match bin.chars().nth(0).unwrap()	{
		'0' => true,
		'1' => false,
		_ => unreachable!()
	};
	return (is_final, &bin[1..5]);
}

fn parse_literal<'a, 'b>(mut bin: &'b str, ops: &'a mut Vec<Op>) -> &'b str	{
	let mut final_packet = false;
	let mut number = "".to_string();

	while !final_packet	{
		let result = parse_part(&bin);
		final_packet = result.0;
		number.push_str(result.1);
		bin = &bin[5..];
	}

	ops.push(Op::Value(bin_to_int(&number)));

	return bin;
}

fn parse_version<'a, 'b>(bin: &'b str, ops: &'a mut Vec<Op>) -> &'b str	{
	let new_bin = parse_type(&bin[3..], ops);
	return new_bin;
}

fn parse_type<'a, 'b>(bin: &'b str, ops: &'a mut Vec<Op>) -> &'b str	{
	let bit_string = &bin[..3];
	let id = bin_to_int(bit_string);
	if id == 4	{
		return parse_literal(&bin[3..], ops);
	}
	let op = match id	{
		0 => Op::Sum,
		1 => Op::Product,
		2 => Op::Min,
		3 => Op::Max,
		5 => Op::GreaterThan,
		6 => Op::LessThan,
		7 => Op::Equal,
		_ => unreachable!()
	};
	let mut new_ops: Vec<Op> = vec![];
	let new_bin = parse_len_id(&bin[3..], &mut new_ops);
	let operands = new_ops.iter().map(|o| if let Op::Value(v) = *o { v } else { unreachable!() }).collect::<Vec<_>>(); //it is late and i am tired
	let res = match op	{
		Op::Sum => operands.iter().fold(0, |acc, v| acc+v),
		Op::Product => operands.iter().fold(1, |acc, v| acc*v),
		Op::Min => operands.iter().map(|v| *v).min().unwrap(),
		Op::Max => operands.iter().map(|v| *v).max().unwrap(),
		Op::GreaterThan => if operands[0] > operands[1] { 1 } else { 0 },
		Op::LessThan => if operands[0] < operands[1] { 1 } else { 0 },
		Op::Equal => if operands[0] == operands[1] { 1 } else { 0 },
		_ => unreachable!()
	};
	ops.push(Op::Value(res));
	return new_bin;
}

fn bin_to_int(bin: &str) -> usize	{
	usize::from_str_radix(bin, 2).unwrap()
}
