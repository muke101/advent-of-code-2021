use std::path::Path;
use std::fs::read_to_string;
use std::collections::{HashMap, HashSet};

#[derive(Copy, Clone, Hash, Eq, PartialEq)]
enum Digit  {
    Zero,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine
}

pub fn eightp1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-8-test";
    }
    else    {
        name = "./inputs/input-8";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines();
    let digits = HashMap::from([
        (2, vec![Digit::One]), (3, vec![Digit::Seven]), (4, vec![Digit::Four]),
        (5, vec![Digit::Two, Digit::Three, Digit::Five]),
        (6, vec![Digit::Zero, Digit::Six, Digit::Nine]), (7, vec![Digit::Eight])
    ]);
    let mut occourences = 0;

    for line in input_lines	{
		let input_segments = line.split("|").collect::<Vec<_>>();
		let output = input_segments[1].split_whitespace().collect::<Vec<_>>();

		for number in output	{
			match digits.get(&number.len()).unwrap()	{
				d if d[0] == Digit::One ||
					d[0] == Digit::Seven ||
					d[0] == Digit::Four ||
					d[0] == Digit::Eight => { occourences += 1; },
				_ => ()
			}
		}
	}

	println!("{}", occourences);
}

struct Patterns	{
	zero: Option<HashSet<char>>,
	one: Option<HashSet<char>>,
	two: Option<HashSet<char>>,
	three: Option<HashSet<char>>,
	four: Option<HashSet<char>>,
	five: Option<HashSet<char>>,
	six: Option<HashSet<char>>,
	seven: Option<HashSet<char>>,
	eight: Option<HashSet<char>>,
	nine: Option<HashSet<char>>,
}

pub fn eightp2(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-8-test";
    }
    else    {
        name = "./inputs/input-8";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines();
	let mut solution = 0;

    for line in input_lines	{
		let input_segments = line.split("|").collect::<Vec<_>>();
		let patterns = input_segments[0].split_whitespace().collect::<Vec<_>>();
		let output = input_segments[1].split_whitespace().map(|p| get_set(p)).collect::<Vec<_>>();
		let mut solved_patterns = Patterns { zero: None, one: None, two: None, three: None, four: None, five: None, six: None, seven: None, eight: None, nine: None };

		solved_patterns.one = Some(get_set_filter(&patterns, 2));
		solved_patterns.seven = Some(get_set_filter(&patterns, 3));
		solved_patterns.four = Some(get_set_filter(&patterns, 4));
		solved_patterns.eight = Some(get_set_filter(&patterns, 7));

		let len_fives = patterns.iter().filter(|p| p.len() == 5).map(|p| get_set(p)).collect::<Vec<_>>();
		let len_sixes = patterns.iter().filter(|p| p.len() == 6).map(|p| get_set(p)).collect::<Vec<_>>();

		for pattern in len_sixes	{
			let one = solved_patterns.one.as_ref().unwrap();
			let four = solved_patterns.four.as_ref().unwrap();
			if one.difference(&pattern).collect::<Vec<_>>().len() == 1	{
				solved_patterns.six = Some(pattern);
			}
			else if four.difference(&pattern).collect::<Vec<_>>().len() == 1	{
				solved_patterns.zero = Some(pattern);
			}
			else 	{
				solved_patterns.nine = Some(pattern);
			}
		}

		for pattern in len_fives	{
			let six = solved_patterns.six.as_ref().unwrap();
			let one = solved_patterns.one.as_ref().unwrap();
			if pattern.is_subset(six)	{
				solved_patterns.five = Some(pattern);
			}
			else if one.is_subset(&pattern)	{
				solved_patterns.three = Some(pattern);
			}
			else 	{
				solved_patterns.two = Some(pattern);
			}
		}

		let mut numbers: Vec<usize> = vec![];
		for signal in output	{
			let decoded_output = match signal.len()	{
				2 => 1,
				3 => 7,
				4 => 4,
				7 => 8,
				5 => match signal	{
					s if s.difference(solved_patterns.five.as_ref().unwrap()).collect::<Vec<_>>().len() == 0 => 5,
					s if s.difference(solved_patterns.two.as_ref().unwrap()).collect::<Vec<_>>().len() == 0 => 2,
					s if s.difference(solved_patterns.three.as_ref().unwrap()).collect::<Vec<_>>().len() == 0 => 3,
					_ => panic!("malformed input")
				},
				6 => match signal	{
					s if s.difference(solved_patterns.six.as_ref().unwrap()).collect::<Vec<_>>().len() == 0 => 6,
					s if s.difference(solved_patterns.nine.as_ref().unwrap()).collect::<Vec<_>>().len() == 0 => 9,
					s if s.difference(solved_patterns.zero.as_ref().unwrap()).collect::<Vec<_>>().len() == 0 => 0,
					_ => panic!("malformed input")
				},
				_ => panic!("malformed input")
			};
			numbers.push(decoded_output);
		}
		solution += numbers.iter().map(|d| d.to_string()).collect::<String>().parse::<usize>().unwrap();
	}
	println!("{}", solution);
}

fn get_set_filter(pattern: &Vec<&str>, len: usize) -> HashSet<char>	{
	let tmp = pattern.iter().filter(|p| p.len() == len).map(|p| *p).collect::<String>();
	return tmp.chars().collect::<HashSet<_>>();
}

fn get_set(pattern: &str) -> HashSet<char>	{
	return pattern.chars().collect::<HashSet<_>>();
}
