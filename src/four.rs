use std::path::Path;
use std::fs::read_to_string;
use std::collections::HashMap;
use std::str::Lines;

#[derive(Clone, Debug)]
#[derive(PartialEq, Eq)]
struct Cell  {
    number: usize,
    marked: bool
}

#[derive(Clone, Debug)]
#[derive(PartialEq, Eq)]
struct Row  {
    row: Vec<Cell>,
    count: u8
}

#[derive(Clone, Debug)]
#[derive(PartialEq, Eq)]
struct Board    {
    board: Vec<Row>,
    number_map: HashMap<usize, (usize, usize)>,
    column_counts: Vec<usize>
}

fn parse_boards(lines: Lines) -> Vec<Board>   {
    let mut boards: Vec<Board> = vec![];
    let mut rows: Vec<Row> = vec![];
    let mut in_board = false;
    for line in lines   {
        if line != ""   {
            in_board = true;
        }
        else if line == ""  {
            in_board = false;
            let mut new_board = Board{board: rows.clone(), number_map: HashMap::new(), column_counts: vec![0; rows.len()]};
            for (y, row) in new_board.board.iter().enumerate()  {
                for (x, n) in row.row.iter().enumerate() {
                    new_board.number_map.insert(n.number, (y, x));
                }
            }
            boards.push(new_board);
            rows = vec![];
        }
        if in_board {
            rows.push(Row{row: line.split_whitespace()
                          .map(|s| Cell{ number: s.parse::<usize>().unwrap(), marked: false })
                          .collect::<Vec<_>>(), count: 0});
        }
    }

    return boards;
}

pub fn fourp1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-4-test";
    }
    else    {
        name = "./inputs/input-4";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut lines = input.lines();
    let draws = lines.next().unwrap().split(",").map(|n| n.parse::<usize>().unwrap()).collect::<Vec<_>>();
    lines.next();
    let mut boards = parse_boards(lines);

    let mut winning_score = 0;
    'outer: for draw in draws   {
        for board in &mut boards {
            match board.number_map.get(&draw)    {
                Some((y,x)) => {
                    board.board[*y].count += 1;
                    board.column_counts[*x] += 1;
                    board.board[*y].row[*x].marked = true;
                    if board.board[*y].count == 5 || board.column_counts[*x] == 5   {
                        for row in board.board.iter()   {
                            for cell in row.row.iter()  {
                                if !cell.marked  {
                                    winning_score += cell.number;
                                }
                            }
                        }
                        winning_score *= draw;
                        break 'outer
                    }
                },
                None => ()
            }
        }
    }
    println!("{}", winning_score);
}

pub fn fourp2(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-4-test";
    }
    else    {
        name = "./inputs/input-4";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut lines = input.lines();
    let draws = lines.next().unwrap().split(",").map(|n| n.parse::<usize>().unwrap()).collect::<Vec<_>>();
    lines.next();
    let mut boards = parse_boards(lines);

    let total_boards = boards.len();
    let mut completed_boards = 0;
    let mut losing_score = 0;
    let mut c;
    let mut completed: Vec<usize> = vec![];
    'outer: for draw in draws   {
        c = 0;
        for board in boards.iter_mut() {
            if completed.contains(&c)    {
                c += 1;
                continue;
            }
            match board.number_map.get(&draw)    {
                Some((y,x)) => {
                    if board.board[*y].count != 5 && board.column_counts[*x] != 5   {
                        board.board[*y].count += 1;
                        board.column_counts[*x] += 1;
                        board.board[*y].row[*x].marked = true;
                        if board.board[*y].count == 5 || board.column_counts[*x] == 5   {
                            completed.push(c);
                            completed_boards += 1;
                            if completed_boards == total_boards {
                                for row in board.board.iter()   {
                                    for cell in row.row.iter()  {
                                        if !cell.marked  {
                                            losing_score += cell.number;
                                        }
                                    }
                                }
                                losing_score *= draw;
                                break 'outer
                            }
                        }
                    }
                },
                None => ()
            }
        c += 1;
        }
    }
    println!("{}", losing_score);
}
