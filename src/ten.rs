use std::path::Path;
use std::fs::read_to_string;
use std::collections::HashMap;

pub fn tenp1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-10-test";
    }
    else    {
        name = "./inputs/input-10";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();

	let mut total_score = 0;
	let bracket_pairs = HashMap::from([(')','('), ('}','{'), (']','['), ('>','<')]);
    for line in input.lines()	{
        let mut stack: Vec<char> = vec![];
        for c in line.chars()	{
			let score;
            match c	{
                '(' | '[' | '{' | '<' => { score = 0; stack.push(c); },
				')' | ']' | '}' | '>' => score = trunc_stack(&mut stack, &bracket_pairs, c),
				_ => panic!("malformed input")
            }
			if score > 0	{
				total_score += score;
				break;
			}
        }
    }
	println!("{}", total_score);
}

fn trunc_stack(stack: &mut Vec<char>, bracket_pairs: &HashMap<char, char>, bracket: char) -> usize	{
	if stack[stack.len()-1] == bracket_pairs[&bracket]	{
		stack.pop();
		return 0;
	}
	else {
		return match bracket	{
			')' => 3,
			']' => 57,
			'}' => 1197,
			'>' => 25137,
			_ => panic!("rust is broken")
		};
	}
}

pub fn tenp2(is_test: bool)	{
    let name: &str;
    if is_test  {
        name = "./inputs/input-10-test";
    }
    else    {
        name = "./inputs/input-10";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();

	let reverse_bracket_pairs = HashMap::from([(')','('), ('}','{'), (']','['), ('>','<')]);
	let bracket_pairs = HashMap::from([('(',')'), ('{','}'), ('[',']'), ('<','>')]);
	let mut scores: Vec<usize> = vec![];
    for line in input.lines()	{
        let mut stack: Vec<char> = vec![];
		let mut corrupted = 0;
        for c in line.chars()	{
            match c	{
                '(' | '[' | '{' | '<' => stack.push(c),
				')' | ']' | '}' | '>' => if stack[stack.len()-1] == reverse_bracket_pairs[&c] { stack.pop(); } else { corrupted = 1; break; }
				_ => panic!("malformed input")
            }
        }
		if corrupted == 1	{
			continue;
		}
		let mut score = 0;
		for c in stack.iter().rev()	{
			score *= 5;
			score += match bracket_pairs[&c]	{
				')' => 1,
				']' => 2,
				'}' => 3,
				'>' => 4,
				_ => panic!("rust is broken")
			};
		}
		scores.push(score);
    }

	scores.sort_unstable();
	println!("{}", scores[scores.len()/2]);
}
