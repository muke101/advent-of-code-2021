use std::path::Path;
use std::fs::read_to_string;

pub fn onep1(is_test: bool) {
    let name: &str;
    if is_test  {
        name = "./inputs/input-1-test";
    }
    else    {
        name = "./inputs/input-1";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines()
                           .map(|s| s.parse::<usize>().unwrap())
                           .collect::<Vec<_>>();
    let mut last_value = input_lines[0];
    let mut number_of_increases = 0;
    for i in input_lines[1..].iter()    {
        if *i > last_value  {
            number_of_increases += 1;
        }
        last_value = *i;
    }
    println!("{}", number_of_increases);
}

pub fn onep2(is_test: bool) {
    let name: &str;
    if is_test  {
        name = "./inputs/input-1-test";
    }
    else    {
        name = "./inputs/input-1";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let input_lines = input.lines()
                           .map(|s| s.parse::<usize>().unwrap())
                           .collect::<Vec<_>>();
    let mut number_of_increases = 0;
    let mut last_sum: usize = input_lines[0..3].iter().sum();
    for i in 1..input_lines.len()   {
        let window: usize = input_lines[i..i+3].iter().sum();
        println!("{}, {}", window, last_sum);
        if window > last_sum    {
            number_of_increases += 1;
        }
        last_sum = window;
        if i == input_lines.len() - 3   {
            break;
        }
    }
    println!("{}", number_of_increases);
}
