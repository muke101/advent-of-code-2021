use std::path::Path;
use std::fs::read_to_string;

pub fn twentyp1(is_test: bool)   {
    let name: &str;
    if is_test  {
        name = "./inputs/input-20-test";
    }
    else    {
        name = "./inputs/input-20";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut input_lines = input.lines();
    let enhancement = input_lines.next()
								 .unwrap()
								 .chars()
								 .map(|c| match c { '#' => 1, '.' => 0, _ => unreachable!() })
								 .collect::<Vec<u8>>();
    input_lines.next().unwrap();
    let image = input_lines.map(|l| l.chars()
								.map(|c| match c { '#' => 1, '.' => 0, _ => unreachable!() })
								.collect::<Vec<u8>>())
						   .collect::<Vec<_>>();
    let x_max = image[0].len();

    let mut enhanced_image = image;

	for i in 0..50	{
		enhanced_image = enhance(&enhancement, enhanced_image, x_max+(i*2), i as u8);
	}

	println!("{}", enhanced_image.iter()
			 .map(|l| l.iter()
				  .filter(|i| **i == 1)
				  .map(|n| *n)
				  .collect::<Vec<u8>>()
				  .iter()
				  .sum::<u8>())
			 .map(|n| n as u32)
			 .collect::<Vec<u32>>()
			 .iter()
			 .sum::<u32>());
}

fn enhance(enhancement: &Vec<u8>, original_image: Vec<Vec<u8>>, x_max: usize, iteration: u8) -> Vec<Vec<u8>>	{ //computer, enhance
	let lower_padding = 2;
	let upper_padding = x_max+2;
	let mut new_image = vec![vec![iteration % 2; lower_padding+upper_padding]; lower_padding+upper_padding];
	new_image[lower_padding as usize..upper_padding].iter_mut()
													.enumerate()
													.for_each(|(c,l)|
															  l[lower_padding as usize..upper_padding]
															  .copy_from_slice(&original_image[c]));
	let mut enhanced_image = new_image.clone();

	for y in lower_padding-1..upper_padding+1	{
		for x in lower_padding-1..upper_padding+1	{
			enhanced_image[y][x] = enhancement[convolve(&new_image, y, x)];
		}
	}

	return enhanced_image[lower_padding-1..upper_padding+1].iter().map(|l| l[lower_padding-1..upper_padding+1].iter().map(|n| *n).collect::<Vec<_>>()).collect::<Vec<_>>()
}

fn convolve(image: &Vec<Vec<u8>>, y: usize, x: usize) -> usize	{
	let points = [(x-1,y-1),(x,y-1),(x+1,y-1),
				  (x-1,y),(x,y),(x+1,y),(x-1,y+1),
				  (x,y+1),(x+1,y+1)];
	let bit_string = points.iter().map(|(x,y)| std::char::from_digit(image[*y][*x].into(), 2).unwrap()).collect::<String>();
	return usize::from_str_radix(&bit_string, 2).unwrap();
}
