use std::path::Path;
use std::fs::read_to_string;
use std::collections::LinkedList;

pub fn eleven(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-11-test";
    }
    else    {
        name = "./inputs/input-11";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut grid = input.lines().map(|l| l.chars()
                                 .map(|c| c.to_digit(10).unwrap())
                                 .collect::<Vec<_>>())
                            .collect::<Vec<_>>();
    let y_len = 10;
    let x_len = 10;
	let mut flashes: LinkedList<(usize, usize)> = LinkedList::new();

	let mut n = 1;
	loop   {
		let mut flash_count = 0;
		for y in 0..y_len	{
			for x in 0..x_len	{
				grid[y][x] = (grid[y][x] + 1) % 10;
				if grid[y][x] == 0	{ flash_count += 1; flashes.push_back((x, y)); }
			}
		}
		while flashes.len() > 0	{
			for (x, y) in flashes.pop_front()	{
				for (x_n, y_n) in get_neighbors(x as isize, y as isize, x_len as isize, y_len as isize)	{
					if grid[y_n][x_n] == 0	{ continue; }
					grid[y_n][x_n] = (grid[y_n][x_n] + 1) % 10;
					if grid[y_n][x_n] == 0	{ flash_count += 1; flashes.push_back((x_n, y_n)); }
				}
			}
		}
		if flash_count == x_len*y_len	{
			println!("{}", n);
			break;
		}
		n += 1;
	}
}

fn get_neighbors(x: isize, y: isize, x_len: isize, y_len: isize) -> Vec<(usize, usize)>	{
	let diagonals = vec![(x+1,y),(x,y+1),(x+1,y+1),(x-1,y),
						 (x,y-1),(x-1,y-1),(x-1,y+1),(x+1,y-1)];
	return diagonals.iter()
					.filter(|(x,y)| *x >= 0 && *x <= x_len-1 && *y >= 0 && *y <= y_len-1)
					.map(|(x,y)| (*x as usize,*y as usize))
					.collect::<Vec<_>>();
}
