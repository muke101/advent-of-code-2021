use std::path::Path;
use std::fs::read_to_string;

pub fn threep1(is_test: bool)   {
    let name: &str;
    if is_test  {
        name = "./inputs/input-3-test";
    }
    else    {
        name = "./inputs/input-3";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let len = input.lines().next().unwrap().len();
    let mut result = vec![0; len];
    for number in input.lines() {
        for (c, digit) in number.chars().enumerate()  {
            let i = digit.to_digit(2).unwrap();
            result[c] += i8::pow(-1, i);
        }
    }
    let most_common = result.iter()
                            .map(|d|
                                 match *d {
                                     i if i < 0 => "1",
                                     i if i > 0 => "0",
                                     _ => panic!("equal number of 1's and 0's")
                                 })
                            .collect::<String>();
    let most_common_decimal = usize::from_str_radix(most_common.as_str(), 2).unwrap();
    let bit_length = if is_test { 5 } else { 12 };
    let mask = !((!0 as usize) << bit_length);
    let least_common = !(most_common_decimal) & mask;
    println!("{}", most_common_decimal*least_common);
}

pub fn threep2(is_test: bool)   {
    let name: &str;
    if is_test  {
        name = "./inputs/input-3-test";
    }
    else    {
        name = "./inputs/input-3";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let numbers = input.lines().collect::<Vec<&str>>();
    let bit_length = if is_test { 5 } else { 12 };
    let o2_inds = (0..numbers.len()).collect::<Vec<_>>();
    let co2_inds = (0..numbers.len()).collect::<Vec<_>>();
    let o2_ind = count(o2_inds, "O2", bit_length, &numbers);
    let co2_ind = count(co2_inds, "CO2", bit_length, &numbers);
    let o2 = numbers[o2_ind];
    let co2 = numbers[co2_ind];
    let o2_decimal = usize::from_str_radix(o2, 2).unwrap();
    let co2_decimal = usize::from_str_radix(co2, 2).unwrap();
    println!("{}, {}", o2_decimal, co2_decimal);
    println!("{}", o2_decimal*co2_decimal);
}

fn count(mut inds: Vec<usize>, mode: &str, bit_length: usize, numbers: &Vec<&str>) -> usize    {

    for pos in 0..bit_length    {

        let mut result = 0;
        let mut most_common = 0;
        let mut least_common = 0;
        for i in inds.iter()   {
            let i = numbers[*i].chars().nth(pos).unwrap().to_digit(2).unwrap();
            result += i8::pow(-1, i);
        }

        if result > 0   {
            least_common += 1;
        }
        else { //captures the behavior of a tie when doing O2 and C02 at the same time (probably)
            most_common += 1;
        }

        let keep_bit = if mode == "O2" { most_common } else { least_common };

        inds = inds.iter()
                .filter(|i|
                        numbers[**i].chars()
                        .nth(pos)
                        .unwrap()
                        .to_digit(2)
                        .unwrap() == keep_bit)
                .map(|i| *i)
                .collect::<Vec<_>>();
        if inds.len() == 1  {
            return inds[0];
        }
    }
    panic!("didn't find single number");
}
