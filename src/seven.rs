use std::path::Path;
use std::fs::read_to_string;
use math::round;

pub fn sevenp1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-7-test";
    }
    else    {
        name = "./inputs/input-7";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut positions = input.strip_suffix("\n").unwrap().split(",").map(|c| c.parse::<u32>().unwrap()).collect::<Vec<_>>();

    positions.sort_unstable();
    let median = positions[round::floor((positions.len() as f32 / 2.0).into(), 1) as usize];
    let mut fuel = 0;
    positions.iter().for_each(|pos| fuel += (median as i32 - *pos as i32).abs());

    println!("{}",fuel);
}

pub fn sevenp2(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-7-test";
    }
    else    {
        name = "./inputs/input-7";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let mut positions = input.strip_suffix("\n").unwrap().split(",").map(|c| c.parse::<u32>().unwrap()).collect::<Vec<_>>();

    positions.sort_unstable();
    let mean = (positions.iter().sum::<u32>() as f64 / positions.len() as f64).floor() as u32;
    let mut fuel = 0;
    positions.iter().for_each(|pos| fuel += addition_factorial(((*pos as i32 - mean as i32)).abs()));
    println!("{}",fuel);
}

fn addition_factorial(n: i32) -> u32    {
    ((i32::pow(n, 2) + n) / 2) as u32
}
