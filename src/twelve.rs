use std::path::Path;
use std::fs::read_to_string;
use std::collections::HashMap;
use itertools::Itertools;

#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug)]
enum Cave<'a>   {
    Big(&'a str),
    Small(&'a str)
}

pub fn twelvep1(is_test: bool)    {
    let name: &str;
    if is_test  {
        name = "./inputs/input-12-test";
    }
    else    {
        name = "./inputs/input-12";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
	let mut graph: Vec<(Cave, Vec<Cave>)> = vec![];
    let mut indexes: HashMap<Cave, usize> = HashMap::new();
    let mut path_map: HashMap<Cave, Vec<Vec<Cave>>> = HashMap::new();

	for edge in input.lines()	{
		let (beginning, destination) = edge.split("-")
										   .map(|c| get_cave_type(c))
										   .collect_tuple()
										   .unwrap();
		match indexes.get(&beginning)	{
			Some(i) => graph[*i].1.push(destination),
			None => { graph.push((beginning, vec![destination]));
					  indexes.insert(beginning, graph.len()-1); }
		}
		match indexes.get(&destination)	{
			Some(i) => graph[*i].1.push(beginning),
			None => { graph.push((destination, vec![beginning]));
					  indexes.insert(destination, graph.len()-1); }
		}
	}

	let mut node_stack: Vec<&(Cave, Vec<Cave>)> = vec![];
	//let mut small_cave_count: HashMap<&Vec<Cave>, usize> = HashMap::new();
	let start_node = graph.iter()
						  .filter(|n| match n.0 { Cave::Small("start") => true, _ => false })
						  .collect::<Vec<_>>()[0];
	node_stack.push(start_node);
    path_map.insert(start_node.0, vec![vec![start_node.0]]);

	while node_stack.len() > 0	{
        for (node, edges) in node_stack.pop()	{
            let paths = path_map.get(&node).unwrap().clone();
            for (c, edge) in edges.iter().enumerate()	{
				let mut new_paths: Vec<Vec<Cave>> = vec![];
				for path in paths.iter()	{
					let mut new_path = match c	{
						c if c < path.len()-1 => path.clone(),
						_ => path.to_vec()
					};
					new_path.push(*edge);
					if is_valid_path(&new_path) { new_paths.push(new_path); }
				}
				if new_paths.len() == 0 { continue; }
				match path_map.get_mut(&edge)	{
					Some(v) => { v.append(&mut new_paths); },
					None => { path_map.insert(*edge, new_paths); }
				}
				if let Cave::Small("end") = edge { continue; }
				let graph_index = indexes.get(edge).unwrap();
				node_stack.push(&graph[*graph_index]);
            }
			path_map.get_mut(&node).unwrap().clear();
        }
	}

	println!("{}", path_map.get(&Cave::Small("end")).unwrap().len());

}

fn is_valid_path(path: &Vec<Cave>) -> bool	{
	let mut visits: HashMap<Cave, u8> = HashMap::new();
	for cave in path.iter()	{
		if let Cave::Small(_) = cave	{
			visits.entry(*cave).and_modify(|v| *v += 1).or_insert(1);
			match cave	{
				Cave::Small("start") => if visits[cave] > 1 { return false; },
				_ => if visits[cave] > 2 { return false; }
			}
		}
	}
	return visits.values().filter(|v| **v > 1).collect::<Vec<_>>().len() <= 1;
}

fn get_cave_type(cave: &str) -> Cave	{
	match cave	{
		c if c.chars().next().unwrap().is_uppercase() => Cave::Big(cave),
		c if c.chars().next().unwrap().is_lowercase() => Cave::Small(cave),
		_ => panic!("malformed input")
	}
}
