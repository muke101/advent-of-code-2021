use std::path::Path;
use std::fs::read_to_string;

pub fn six(is_test: bool, length: usize) {
    let name: &str;
    if is_test  {
        name = "./inputs/input-6-test";
    }
    else    {
        name = "./inputs/input-6";
    }
    let p = Path::new(name);
    let input = read_to_string(p).unwrap();
    let fish = input.strip_suffix("\n").unwrap().split(",").map(|c| c.parse::<u8>().unwrap()).collect::<Vec<_>>();
    let mut total_fish = fish.len();
    let mut days: Vec<usize> = vec![0; length+1];

    fish.iter().for_each(|f| days[(*f - 1) as usize] += 1);

    for day in 0..length    {
        let number_of_spawns = days[day];
        if day < length - 7 { days[day+7] += number_of_spawns; }
        if day < length - 9 { days[day+9] += number_of_spawns; }
        total_fish += number_of_spawns;
    }

    println!("{}", total_fish);
}
