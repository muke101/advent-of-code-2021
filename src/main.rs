use std::env;

mod twenty;
mod eighteen;
mod seventeen;
mod sixteen;
mod fithteen;
mod fourteen;
mod thirteen;
mod one;
mod two;
mod three;
mod four;
mod five;
mod six;
mod seven;
mod eight;
mod nine;
mod ten;
mod eleven;
mod twelve;

fn main() {
    let args: Vec<String> = env::args().collect();
    let is_test = match args.len()  {
        3 => true,
        _ => false
    };
    match args[1].as_str()    {
        "1p1" => {one::onep1(is_test)},
        "1p2" => {one::onep2(is_test)},
        "2p1" => {two::twop1(is_test)},
        "2p2" => {two::twop2(is_test)},
        "3p1" => {three::threep1(is_test)},
        "3p2" => {three::threep2(is_test)},
        "4p1" => {four::fourp1(is_test)},
        "4p2" => {four::fourp2(is_test)},
        "5p1" => {five::fivep1(is_test)},
        "5p2" => {five::fivep2(is_test)},
        "6p1" => {six::six(is_test, 79)},
        "6p2" => {six::six(is_test, 9999999-1)},
        "7p1" => {seven::sevenp1(is_test)},
        "7p2" => {seven::sevenp2(is_test)},
        "8p1" => {eight::eightp1(is_test)},
        "8p2" => {eight::eightp2(is_test)},
        "9p1" => {nine::ninep1(is_test)},
        "9p2" => {nine::ninep2(is_test)},
        "10p1" => {ten::tenp1(is_test)},
        "10p2" => {ten::tenp2(is_test)},
        "11p1" => {eleven::eleven(is_test)},
        "11p2" => {eleven::eleven(is_test)},
        "12" => {twelve::twelvep1(is_test)},
        "13p1" => {thirteen::thirteenp1(is_test)},
        "13p2" => {thirteen::thirteenp2(is_test)},
        "14" => {fourteen::fourteen(is_test)},
        "15p1" => {fithteen::fithteenp1(is_test)},
        "15p2" => {fithteen::fithteenp2(is_test)},
        "16" => {sixteen::sixteen(is_test)},
        "17p1" => {seventeen::seventeenp1(is_test)},
        "17p2" => {seventeen::seventeenp2(is_test)},
        "18p1" => {eighteen::eighteenp1(is_test)},
        "20p1" => {twenty::twentyp1(is_test)},
        _ => panic!("malformed input")
    }
}
